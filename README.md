# Adding or Modifying an External Package

* Checkout the master branch
* Add downloaded archive file under `source/<package-name>`
* Create build script (preferrably named "build.py") that
  "installs" the package into `packages/<platform-id>/debug/<package-name>`
  and `packages/<platform-id>/release/<package-name>`.
* Check all this into the master branch with a commit messages that looks like
  "<package-name> <version> source" or "<package-name> <version> <platform-id>
  release".
* Now checkout the <platform-id>/release branch
* And fetch the files from the master branch with the command:
  `git checkout master -- packages/<platform-id>/release/<package-name>`
* Move the files to the top-level, removing the existing package if it's
  already there first: `mv packages/<platform-id>/release/<package-name>
  ./<package-name>`.
* Add everything and commit to the `<platform-id>/release` branch. The commit
  should look like only files under the `./<package-name>` directory changed.

* Now you may update the parent project that uses these external packages.
