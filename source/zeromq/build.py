import multiprocessing
import os
import shutil
import subprocess

import platform
import distro


def platform_id():
    system = platform.system().lower()
    if system == 'linux':
        name = distro.id()
        version = distro.version().split('.')[0]
        return f'{system}-{name}{version}'
    elif system == 'darwin':
        version = '.'.join(platform.mac_ver()[0].split('.')[:2])
        return f'mac{version}'
    elif system == 'windows':
        version = distro.version()
        return f'win{version}'
    else:
        return 'unknown'

here = os.path.abspath(os.path.dirname(__file__))
externals = os.path.dirname(os.path.dirname(here))
outdir = os.path.join(externals, 'packages')

shutil.unpack_archive('zeromq-4.3.2.tar.gz', '.')

for build_type in ['Release', 'Debug']:
    install_prefix = os.path.join(outdir, platform_id(), build_type.lower(), 'zeromq')
    cmds = [
        ['cmake', '../zeromq-4.3.2',
         '-D', 'CMAKE_INSTALL_PREFIX=' + install_prefix,
         '-D', 'WITH_PERF_TOOL=OFF',
         '-D', 'ZMQ_BUILD_TESTS=OFF',
         '-D', 'CMAKE_BUILD_TYPE=' + build_type],
        ['cmake', '--build', '.', '-j', str(multiprocessing.cpu_count())],
        ['cmake', '--install', '.'],
    ]
    buildir = 'build_' + build_type.lower()
    os.makedirs(buildir)
    for cmd in cmds:
        proc = subprocess.Popen(cmd, env=os.environ, cwd=buildir)
        if proc.wait() != 0:
            raise RuntimeError

shutil.rmtree('build_release')
shutil.rmtree('build_debug')
shutil.rmtree('zeromq-4.3.2')
