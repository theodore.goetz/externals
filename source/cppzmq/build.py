import multiprocessing
import os
import shutil
import subprocess

import platform
import distro


def platform_id():
    system = platform.system().lower()
    if system == 'linux':
        name = distro.id()
        version = distro.version().split('.')[0]
        return f'{system}-{name}{version}'
    elif system == 'darwin':
        version = '.'.join(platform.mac_ver()[0].split('.')[:2])
        return f'mac{version}'
    elif system == 'windows':
        version = distro.version()
        return f'win{version}'
    else:
        return 'unknown'

here = os.path.abspath(os.path.dirname(__file__))
externals = os.path.dirname(os.path.dirname(here))
outdir = os.path.join(externals, 'packages', platform_id())

shutil.unpack_archive('cppzmq-v4.6.0.tar.gz', '.')

for build_type in ['Release', 'Debug']:
    install_prefix = os.path.join(outdir, build_type.lower(), 'cppzmq')
    prefix_path = os.path.join(outdir, build_type.lower(), 'zeromq')
    cmds = [
        ['cmake', '../cppzmq-4.6.0',
         '-D', 'CMAKE_PREFIX_PATH=' + os.path.normpath(prefix_path),
         '-D', 'CMAKE_INSTALL_PREFIX=' + install_prefix,
         '-D', 'CPPZMQ_BUILD_TESTS=OFF',
         '-D', 'CMAKE_BUILD_TYPE=' + build_type],
        ['cmake', '--build', '.', '-j', str(multiprocessing.cpu_count())],
        ['cmake', '--install', '.'],
    ]
    buildir = 'build_' + build_type.lower()
    os.makedirs(buildir)
    for cmd in cmds:
        proc = subprocess.Popen(cmd, env=os.environ, cwd=buildir)
        if proc.wait() != 0:
            raise RuntimeError

shutil.rmtree('build_release')
shutil.rmtree('build_debug')
shutil.rmtree('cppzmq-4.6.0')
